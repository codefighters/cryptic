Cryptic
====================

Esta biblioteca foi feita com o intuito de disponibilizar métodos de encriptação, 
que possibilitam a transmissão de informações de forma segura.

Um dos grandes objetivos é ser extramente simples de utilizar, 
sendo que pode até ser facilmente utilizada através da linha de comandos.

__NOTA:__ Necessita de [Composer](https://getcomposer.org).




## Utilização de composer

Para utilização rápida de Composer basta definir um ficheiro "composer.json", como 
o exemplificado abaixo.

```JSON
{
    "require": {      
        "codefighters/cryptic": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/codefighters/cryptic.git"
        }
    ]
}
```
Seguidamente, utilizar o composer para resolver a dependência.

```console
foo@bar:~$ composer install
```

- - -

Tipos disponíveis
================================

Os tipos de encriptação disponíveis misturam várias técnicas e métodos de 
encriptação de modo a possibilitar uma comunicação segura.

A sua utilização é feita através de invocação CripterFactory. Esta classe
permite gerir o tipo de encriptação pronto a ser utilizado.

```PHP
//autoloader de PHP Composer
require_once( realpath(__DIR__)."/../vendor/autoload.php");

use Cryptic\CrypticFactory;

$options = array(
    'type'              => 'tipo',
    'outrosElemento1'   => 'info1',
    'outrosElementoN'   => 'infoN'
);

$factory = new CrypticFactory();
$crypter = $factory->build($options);

$crypter->funcao();
```


 
## Tipo CrypterOne

Permite a encriptação através do método AES-256-CBC em conjunto com
a utilização de hash MD5 e BCRYPT.

Este método assume uma password previamente partilhada entre ambos os elementos
 aos quais se pretende efetuar a comunicação. Seguidamente basta enviar para o 
 destinatário:
 
  * **Mensagem encriptada**: conteudo encriptado que se pretende transmitir;
  
  * **Key**: Chave que conjuntamente com a password permite e desencriptação;
 
 Estes últimos elementos obtidos da função **encrypt**.
 
**Exemplo de utilização:**

```PHP

//autoloader de PHP Composer
//Corrigir o caminho para o ficheiro autoload.php se necessário
require_once( realpath(__DIR__)."/../vendor/autoload.php");

use Cryptic\CrypticFactory;

$options = array(
    'type'      => 'CrypterOne',
    'password'  => 'gthb3sc3RLrpd1798374',
);

$factory = new CrypticFactory();
$crypter = $factory->build($options);

$crypticResult = $crypter->encrypt('A informação a manter sigilosa');

var_dump($crypticResult);

var_dump($crypter->decrypt( $crypticResult['encryptedMessage'], $crypticResult['key'] ));
```
  
  
  
### Demonstração de CrypterOne

Forma simplificada, de modo a ser implementada noutras linguagens.
**NOTA**: Este pequeno teste é apenas uma forma simplificada de resolução,
pelo que não deve ser utilizado em produção. 

```PHP
$mensagemEnviar         = 'Vou passar uma mensagem e ja está!';
$password               = 'gthb3sc3RLrpd1798374';
$method                 = 'aes-256-cbc';

$key                    = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);
$iv                     = substr( md5($key) , 0, 16);
$generatedKey           = md5($password.$key);

$messageEncriptada      = base64_encode( openssl_encrypt($mensagemEnviar, $method, $generatedKey, OPENSSL_RAW_DATA, $iv ) );

$mensagemDesencriptada  = openssl_decrypt( base64_decode($messageEncriptada), $method, $generatedKey, OPENSSL_RAW_DATA, $iv );

echo '<p>Mensagem a Enviar: '.$mensagemEnviar.' </p>';

echo '<p>Parametros partilhados:</p>';
echo '<p>Password:'.$password.'</p>';
echo '<p>Metodo=' . $method . "</p>";
echo '<p></p>';

echo '<p>Parametros a enviar:</p>';
echo '<p>Mensagem Encriptada:'.$messageEncriptada.'</p>';
echo "<p>Key:" . $key . "</p>";
echo '<p></p>';

echo '<p>Resultado de tratamento:</p>';
echo '<p>Mensagem Desencriptada:'.$mensagemDesencriptada.'</p>';
```