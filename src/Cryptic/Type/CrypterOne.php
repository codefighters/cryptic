<?php

namespace Cryptic\Type;

use Cryptic\CrypticException;
use Cryptic\CrypticInterface;

class CrypterOne
    implements CrypticInterface {

    const METHOD = 'aes-256-cbc';
    const PASSWORD_SIZE = 20;

    protected $_values = array();

    public function __construct($password)
    {
        $this->_definePassword($password);
        return $this;
    }


    protected function _definePassword($value) {

        if ( $value && ctype_alnum($value) && strlen($value) == 20 ) {

            $this->_values['password'] = $value;
            return $this;
        }

        throw new CrypticException('A password identificada não corresponde aos critérios aceitáveis.');
    }


    public function getPassword() {

        $key = 'password';
        return isset($this->_values[$key]) ? $this->_values[$key] : null;

    }

    protected function _generateKey($password) {

        return password_hash( $password, PASSWORD_BCRYPT, ['cost' => 12]);;
    }


    protected function _generateHashKeyPassword($key, $password) {

        return md5($password.$key );
    }


    protected function _generateIv($key) {

        return substr( md5($key) , 0, 16);
    }


    public function encrypt($message) {

        $key =  $this->_generateKey($this->getPassword());

        $encryptedMessage = base64_encode(
            openssl_encrypt(
                $message,
                self::METHOD,
                $this->_generateHashKeyPassword($key, $this->getPassword()),
                OPENSSL_RAW_DATA,
                $this->_generateIv($key)
            )
        );

        return array(
            'key'               => $key,
            'encryptedMessage'  => $encryptedMessage,
        );

    }

    public function decrypt($message, $key) {

        return openssl_decrypt(
            base64_decode($message),
            self::METHOD,
            $this->_generateHashKeyPassword($key, $this->getPassword()),
            OPENSSL_RAW_DATA,
            $this->_generateIv($key)
        );
    }

}