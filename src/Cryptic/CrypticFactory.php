<?php

namespace Cryptic;

use Cryptic\Type\CrypterOne;

class CrypticFactory {

    /**
     * @param array $options
     * @return CrypticInterface
     * @throws CrypticException
     */
    public function build(array $options) {

        if ( ! (isset($options['type']) && isset($options['password']) ) ) {

            throw new CrypticException('Necessário identificar as keys type e password');
        }

        switch ($options['type']) {

            case 'CrypterOne':

                if ( !isset($options['password']) ) {

                    throw new CrypticException('O CrypterOne necessita de uma password');
                }

                $product = new CrypterOne($options['password']);
                break;


            default:
                throw new CrypticException('Type de crypter não reconhecido');
                break;

        }

        return $product;

    }

}