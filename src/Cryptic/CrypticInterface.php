<?php

namespace Cryptic;

interface CrypticInterface {

    public function encrypt($message);

    public function decrypt($message, $key);

}